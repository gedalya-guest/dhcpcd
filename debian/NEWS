dhcpcd5 (9.4.1-21) unstable; urgency=medium

   Since Debian 12 (Bookworm), dhcpcd uses Predictable Network Interface Names
   on Linux ports. This brings dhcpcd in line with other networking tools.

   This change is only noticeable on hosts where networking is controlled by
   ifupdown via </etc/network/interfaces>. For example:

   Before:

   iface eth0 inet dhcp
   iface eth0 inet6 auto

   After:

   iface enp4s0 inet dhcp
   iface enp4s0 inet6 auto

   'sudo dmesg | grep -i renamed' shows available Predictable Interface Names.

 -- Martin-Éric Racine <martin-eric.racine@iki.fi>  Sun, 12 Mar 2023 08:26:06 +0200
